<?php

/**
 * Функція __autoload для автоматичного підключення класів
 */
function __autoload($class_name)
{
    // Массив папок, в которых могут находиться необходимые классы
    $array_paths = array(
        '/configs/',
        '/components/',
        '/controllers/',
    );

// Проходимо по масиву папок
    foreach ($array_paths as $path) {

// Формуємо ім'я і шлях до файлу з класом
        $path = ROOT . $path . $class_name . '.php';

// Якщо такий файл існує, підключаємо його
        if (is_file($path)) {
            include_once $path;
        }
    }
}
