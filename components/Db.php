<?php

/**
 * Класс Db
 * Компонент для роботи з базою даних
 */
class Db
{

    /**
     * встановлює з'єднання з бд
     */
    public static function getConnection()
    {

        // отримуємо параменти підключення із файла
        $paramsPath = ROOT . '/configs/bd_params.php';
        $params = include($paramsPath);

        // Вкстановлюємо з'єднання
        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        // Задаємо кодіровку
        $db->exec("set names utf8");

        return $db;
    }

}
