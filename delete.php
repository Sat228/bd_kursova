<?php
session_start();
// підключення файлів системи
define('ROOT', dirname(__FILE__));
require_once(ROOT . '/components/Autoload.php');
?>
<html>
<head>
    <title>магазин продуктів</title>
    <link rel='stylesheet' type='text/css' href='style.css'>
</head>
<body>
<h1><a href='index.htm '>магазин продуктів</a></h1><br><Br>
<table width=100% height=80% valign=top>
    <tr>
        <td width=15% valign=top>
            <p style='text-indent : 0pt; font-size : 12pt'>
                <a href='lookall.php?part=all '> всі товари</a><br><br>
                <a href='lookpost.php '> постачальники продукції</a> <br><br>
                <a href='lookpokup.php '> постійні покупці</a><br><br>
                <a href='lookrab.php '> Працівники магазину</a><br><br><BR>
                <a href='index.htm '> Головна сторінка</a>
            </p>
        </td>
        <td>
            <?php
            $part = $_GET['part'];
            // запам'ятовуємо ідентифікатор, таблицю і ім'я поля запису, яку потрібно видалити
            $id = $_GET['id'];
            $table = $_GET['table'];
            $pole = $_GET['pole'];
            if ($part == "start") //якщо початок процесу видалення
            {
// виводимо прохання на підтвердження видалення
                echo " <h2 > дійсно хочете видалити запис?</h2><br>
<center><form action = 'delete.php' method = get>
<input type = 'submit' value = '&nbsp;&nbsp;&nbsp;&nbsp;так&nbsp;&nbsp;&nbsp;&nbsp;'>
<input type = hidden name = 'part' value = 'go'><input type = hidden name = 'id' value = $id>
<input type = hidden name = 'table' value = $table><input type = hidden name = 'pole' value = $pole>
</form></center>";
            }
            if ($part == "go") // якщо Користувач підтвердив видалення:
            {
// підключаємося до СУБД MsSQL
                $db = Db::getConnection();
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                if ($table == "proizv") // якщо видаляє Постачальника-видаляємо також всі записи про товари, пов'язані з ним
                {
// спочатку видаляємо товари
                    $sql = 'SELECT idp FROM produkt WHERE idpr = \'' . $id . '\' LIMIT 0, 1000';
                    $result = $db->query($sql);
                    $num = $result->rowCount();
                    if ($num != 0) {
                        while ($row = $result->fetch()) {
                            $sql = 'DELETE FROM produkt WHERE idp = \'' . $row[0] . '\' LIMIT 1';
                            $res = $db->query($sql);
                        };
                    }
// звільняємо результати запитів
// потім видалимо запис про постачальника
                    $sql = 'DELETE FROM proizv WHERE idpr = \'' . $id . '\' LIMIT 1';
                    $result = $db->query($sql);
                } else // якщо видаляє не Постачальника
                {
                } // будуємо запит на видалення вибраного запису
                $sql = 'DELETE FROM ' . $table . ' WHERE ' . $pole . ' = \'' . $id . '\' LIMIT 1';
// виконуємо запит
                $result = $db->query($sql);

// ввиводім повідомлення про те, що запис видалено
                echo '<h2 style = "color: red" >запис видалено</h2>';
            }
            ?>
        </td>
        <td width=15% valign=top>
            <p style='text-indent : 0pt; font-size : 12pt'>
                Відділи продажів:<br><BR>
                <a href='lookotdel.php?id=1'>М'ясний відділ</a><br><br>
                <a href='lookotdel.php?id=2'>рибний відділ</a><br><br>
                <a href='lookotdel.php?id=3'>хлібо-булочні вироби</a><br><br>
                <a href='lookotdel.php?id=5'>молочний відділ</a><br><br>
                <a href='lookotdel.php?id=6'>кондитерський відділ</a><br><br>
                <a href='lookotdel.php?id=7'> Бакалія</a><br><br>
                <a href='lookotdel.php?id=8'> напої</a><br><br>
            </p>
        </td>
    </tr>
</table>
</body>
</html>