<?php
session_start();

// підключення файлів системи
define('ROOT', dirname(__FILE__));
require_once(ROOT.'/components/Autoload.php');
?>
<head>
    <title>магазин продуктів</title>
    <link rel = 'stylesheet' type = 'text/css' href = 'style.css'>
</head>
<body>
<h1> <a href='index.htm '>магазин продуктів</a></h1><br><Br>
<table width = 100% height = 80% valign = top>
    <tr>
        <td width = 15% valign = top>
            <p style = 'text-indent : 0pt; font-size : 12pt'>
                <a href = 'lookall.php?part=all ' > всі товари</a><br><br>
                <a href = 'lookpost.php ' > постачальники продукції</a><br><br>
                <a href = 'lookpokup.php ' > постійні покупці</a><br><br>
                <a href = 'lookrab.php ' > Працівники магазину</a><br><br><BR >
                <a href = 'index.php ' > Головна сторінка</a></p>
        </td>
        <td>
            <?php
            // підключаємося до СУБД
            $db = Db::getConnection();
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $part = $_GET['part'];
            if ($part=="all")
            {
// будуємо запит для вибору продуктів, їх виробників і відділу, в якому вони продаються
                $sql = 'SELECT produkt.nazv, produkt.price, produkt.srok, proizv.nazv, proizv.sity, otdel.otdel, produkt.idp FROM produkt, proizv, otdel WHERE proizv.idpr = produkt.idpr AND otdel.ido = proizv.ido LIMIT 0,1000 ';
// виконуємо запит
                $result = $db->query($sql);
// перевіряємо кількість вибраних записів, Якщо 0-виводимо повідомлення про те, що записів Немає, Якщо > 0-виводимо їх
                $num = $result->rowCount();
                if ($num==0)
                {echo ' <h2 > записів немає</h2>';
                    echo '<br><p><a href = "addproduct.php?part=start" > додати новий продукт</a></p>';
                }
                else
                {
                    echo ' <h2 > Список товарів</h2><br>';
// будуємо таблицю, в яку будемо виводити записи
                    echo '<table width = 100% border = 1>
<tr heigth = 7%><th width = 5%><p class = "zag" > № п. п</p></th>
<th width = 27%><p class = "zag">Назва</p></th>
<th width = 10%><p class = "zag">Ціна</P></th>
<th width = 10%><p class = "zag">термін придатності</p></th>
<th width = 27%><p class = "zag">виробник</P></th>
<th width = 15%><p class = " zag " >Відділ продажу</P></th></tr>';
// заповнюємо таблицю поступово обробляючи запит
                    $i = 1;
                    while ($row = $result->fetch())
                    {
                        echo '<tr>';
                        echo "<td><p class = 'small'>$i</p></td>
<td><p class = 'small'>$row[0]</p></td>
<td><p class = 'small'>$row[1]</p></td>
<td><p class = 'small'>$row[2]</p></td>
<td><p class = 'small'>$row[3],<br> $row[4]</p></td>
<td><p class = 'small'>$row[5]</p></td>
<td><p class = 'small'><a href = 'delete.php?part=start&id=$row[6]&table=produkt&pole=idp'>Удалить</a></p></td>
<td><p class = 'small'><a href = 'editproduct.php?part=start&id=$row[7]&table=produkt&pole=idp'>Редагувати</a></p></td>";

                        echo '</tr>';
                        $i++;
                    };
                    echo '</table>';
                    echo '<br><p><a href = "addproduct.php?part=start" > додати новий продукт</a></p>';
                }
            }
            if ($part=="pr")
            {
                $idpr = $_GET['idpr'];
// будуємо запит для вибору продуктів, їх виробників і відділу, в якому вони продаються
                $sql = 'SELECT produkt.nazv, produkt.price, produkt.srok, proizv.nazv, proizv.sity, otdel.otdel, produkt.idp FROM produkt, proizv, otdel WHERE proizv.idpr = produkt.idpr AND otdel.ido = proizv.ido AND produkt.idpr = \''.$idpr.'\' LIMIT 0,1000 ';
// виконуємо запит
                $result = $db->query($sql);
// перевіряємо кількість вибраних записів, Якщо 0-виводимо повідомлення про те, що записів Немає, Якщо > 0-виводимо їх
                $num = $result->rowCount();
                if ($num==0)
                {echo ' <h2 > записів немає</h2>'; }
                else
                {
                    echo ' <h2 > Список товарів</h2><br>';
// будуємо таблицю, в яку будемо виводити записи
                    echo '<table width = 100% border = 1>
<tr heigth = 7%><th width = 5%><p class = "zag" > № п. п</p></th>
<th width = 27%><p class = "zag">Назва</p></th>
<th width = 10%><p class = "zag">Ціна</P></th>
<th width = 10%><p class = "zag">термін придатності</p></th>
<th width = 27%><p class = "zag">виробник</P></th>
<th width = 15%><p class = " zag " >Відділ продажу</P></th></tr>';
// заповнюємо таблицю поступово обробляючи запит
                    $i = 1;
                    while ($row = $result->fetch())
                    {
                        echo '<tr>';
                        echo "<td><p class = 'small'>$i</p></td>
<td><p class = 'small'>$row[0]</p></td>
<td><p class = 'small'>$row[1]</p></td>
<td><p class = 'small'>$row[2]</p></td>
<td><p class = 'small'>$row[3],<br> $row[4]</p></td>
<td><p class = 'small'>$row[5]</p></td>
<td><p class = 'small'><a href = 'delete.php?part=start&id=$row[6]&table=produkt&pole=idp'>Удалить</a></p></td>";
                        echo '</tr>';
                        $i++;
                    };
                    echo '</table>';
                    echo '<br><p><a href = "addproduct.php?part=start" > додати новий продукт</a></p>';
                }
            }
            ?>
        </td>
        <td width = 15% valign = top>
            <p style = 'text-indent : 0pt; font-size : 12pt'>
                Відділи продажів:<br><BR>
                <a href = 'lookotdel.php?id=1'>М'ясний відділ</a><br><br>
                <a href = 'lookotdel.php?id=2'>рибний відділ</a><br><br>
                <a href = 'lookotdel.php?id=3'>хлібо-булочні вироби</a><br><br>
                <a href = 'lookotdel.php?id=5'>молочний відділ</a><br><br>
                <a href = 'lookotdel.php?id=6'>кондитерський відділ</a><br><br>
                <a href = 'lookotdel.php?id=7' > Бакалія</a><br><br>
                <a href = 'lookotdel.php?id=8' > напої</a><br><br>
            </p>
        </td>
    </tr>
</table>
</body>
</html>