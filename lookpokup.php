<?php
session_start();

// підключення файлів системи
define('ROOT', dirname(__FILE__));
require_once(ROOT.'/components/Autoload.php');
?>
<head>
    <title>магазин продуктів</title>
    <link rel = 'stylesheet' type = 'text/css' href = 'style.css'>
</head>
<body>
<h1> <a href='index.htm '>магазин продуктів</a></h1><br><Br>
<table width = 100% height = 80% valign = top>
    <tr>
        <td width = 15% valign = top>
            <p style = 'text-ndent : 0pt; font-size : 12pt'>
                <a href = 'lookall.php?part=all ' > всі товари</a><br><br>
                <a href = 'lookpost.php ' > постачальники продукції</a><br><br>
                <a href = 'lookpokup.php ' > постійні покупці</a><br><br>
                <a href = 'lookrab.php ' > Працівники магазину</a><br><br><BR >
                <a href = 'index.htm ' > Головна сторінка</a></p>
        </td>
        <td>
            <?php
            // підключаємося до СУБД MsSQL
            $db = Db::getConnection();
            // будуємо запит для вибору покупців і відділів, в яких дозволені знижки
            $sql = 'SELECT post_pokup.fio, post_pokup.pasp, post_pokup.skidka, otdel.otdel, post_pokup.id FROM post_pokup,otdel WHERE otdel.ido = post_pokup.ido LIMIT 0,1000 ';
            // виконуємо запит
            $result = $db->query($sql);
            // перевіряємо кількість вибраних записів, Якщо 0-виводимо повідомлення про те, що записів Немає, Якщо > 0-виводимо їх
            $num = $result->rowCount();
            if ($num==0)
            {echo ' <h2 > записів немає</h2>';
                echo '<br><p><a href = "addpokup.php?part=start" > додати нового покупця</a></p>';
            }
            else
            {
                echo ' <h2 > список постійних покупців</h2><br>';
// будуємо таблицю, в яку будемо виводити записи
                echo '<table width = 100% border = 1>
<tr heigth = 7%><th width = 5%><p class = "zag" > № п. п</p></th>
<th width = 27%><p class = " zag " >П. І. Б.</p></th>
<th width = 10%><p class = "zag">номер і серія паспорта</P></th>
<th width = 10%><p class = "zag">знижка (%) </p></th>
<th width = 15%><p class = " zag " >Відділ продажу</P></th></tr>';
// заповнюємо таблицю поступово обробляючи запит
                $i = 1;
                while ($row = $result->fetch())
                {
                    echo '<tr>';
                    echo "<td><p class = 'small'>$i</p></td>
<td><p class = 'small'>$row[0]</p></td>
<td><p class = 'small'>$row[1]</p></td>
<td><p class = 'small'>$row[2]</p></td>
<td><p class = 'small'>$row[3]</p></td>
<td width = 10%><p class = 'small'><a href = 'delete.php?part=start&id=$row[4]&table=post_pokup&pole=id' > видалити</a></p></td>";
                    echo '</tr>';
                    $i++;
                };
                echo '</table>';
                echo '<br><p><a href = "addpokup.php?part=start" > Додати нового покупця</a></p>';
            }
            ?>
        </td>
        <td width = 15% valign = top>
            <p style = 'text-indent : 0pt; font-size : 12pt'>
                Відділи продажів:<br><BR>
                <a href = 'lookotdel.php?id=1'>М'ясний відділ</a><br><br>
                <a href = 'lookotdel.php?id=2'>рибний відділ</a><br><br>
                <a href = 'lookotdel.php?id=3'>хлібо-булочні вироби</a><br><br>
                <a href = 'lookotdel.php?id=5'>молочний відділ</a><br><br>
                <a href = 'lookotdel.php?id=6'>кондитерський відділ</a><br><br>
                <a href = 'lookotdel.php?id=7' > Бакалія</a><br><br>
                <a href = 'lookotdel.php?id=8' > напої</a><br><br>
            </p>
        </td>
    </tr>
</table>
</body>
</html>